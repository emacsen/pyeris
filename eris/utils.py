from math import ceil
from base64 import b32encode
from hashlib import blake2b

def blake2b_digest(data, digest_size=32, encoder=b32encode):
     hash = blake2b(digest_size=digest_size)
     hash.update(data)
     return encoder(hash.digest())

def b32_padlength(unpadded):
    return ceil(len(unpadded) / 8) * 8 - len(unpadded)

def pad(unpadded):
    return unpadded + (b'=' * b32_padlength(unpadded))

def unpad(padded):
    return padded.rstrip(b'=')
