"""This module contains all the core ERIS functionality.

This version uses LevelDB as the block storage
"""
from os import stat
from math import ceil
from io import BytesIO, BufferedReader
from hashlib import blake2b
from Crypto.Util.Padding import pad as crypt_pad
from Crypto.Util.Padding import unpad as crypt_unpad
from Crypto.Cipher import ChaCha20
from plyvel import DB

KEYPAIR_LENGTH = 64

db = DB('eris.db', create_if_missing=True)


def pad(data, block_size):
    """Pad a block of data

    Args:
       data (bytes): The data to be padded
       block_size (int): The size of the data block
    Returns:
       bytes: The padded data
    """
    return crypt_pad(data, block_size, style='iso7816')


def unpad(data, block_size):
    """Unpad a block of data

    Args:
       data (bytes): The data to be unpadded
       block_size (int): The size of the data block
    Returns:
       bytes: The unpadded data
    """
    return crypt_unpad(data, block_size, style='iso7816')


def fill_with_null_rk_pairs(rk_pairs_for_node, arity):
    """Pad a node with blank reference-key pairs

    Args:
        rk_pairs_for_node (bytes): The existing reference-key pairs
        arity (int): How many rk-pairs fit in a block
    Returns:
        bytes: The node padded with null reference-key pairs
    """
    num_of_pairs = arity - len(rk_pairs_for_node)
    nullpair = (b'\0' * 32, b'\0' * 32)
    return rk_pairs_for_node + [nullpair] * num_of_pairs


def encrypt_block(data, convergent_secret):
    """Encrypt data using the convergent secret

    Args:
        data (bytes): The data to be encrypted, ie plaintext
        convergent_secret (bytes): The convergent secret used during hashing
    Returns:
        bytes: The encrypted data, ie cyphertext
    """
    key = blake2b(data, digest_size=32, key=convergent_secret).digest()
    nonce = b'0' * 12
    encrypted_block = ChaCha20.new(key=key, nonce=nonce).encrypt(data)
    reference = blake2b(encrypted_block).digest()
    return encrypted_block, reference, key


def collect_rk_pairs(input_rk_pairs, convergent_secret, block_size):
    """
    Collect the reference key pairs, etc. into a data block.
    This will be run once per level in the tree

    Args:
       input_key_pairs (bytes): reference/key pairs
       convergent_secret (bytes): The convergent secret used during hashing
       block_size (int): The size of the data and node blocks

    Returns:
       tuple: A tuple of data blocks and reference/key pairs
    """

    arity = block_size / KEYPAIR_LENGTH  # The number of references in a file
    if not arity.is_integer():
        raise ValueError(
            f"Block size ({block_size}) not cleanly divisible by " +
            f"keypair length ({KEYPAIR_LENGTH})")
    arity = int(arity)
    blocks = []
    output_rk_pairs = []
    i = 0
    # We take arity number of rk pairs at a time
    while rk_pairs_for_node := input_rk_pairs[i:i+arity]:
        # Fill arity with null reference-key pairs (if necessary)
        rk_pairs_for_node = fill_with_null_rk_pairs(rk_pairs_for_node, arity)
        # Concatenate the rk pairs into a big bytestring
        node = b''.join([b''.join(i) for i in rk_pairs_for_node])
        # Encrypt the now full block
        block, reference, key = encrypt_block(node, convergent_secret)
        blocks.append(block)
        rk_to_node = reference + key
        output_rk_pairs.append(rk_to_node)
        i += arity
    return blocks, output_rk_pairs


def split_content(content, convergent_secret, block_size):
    """Split data into equally sized chunks and encrypt them

    Args:
        content (bytes): The unencrypted data
        convergent_secret (bytes): Convergent secret used during hashing
        block_size (int): The size of the data blocks
    Returns:
        tuple: A tuple consisting of data blocks (list of bytes), the
            corresponding reference-key pairs, which are themselves a
            tuple of the data hash (bytes) and the decryption key (bytes)
    """
    blocks = []
    rk_pairs = []
    if isinstance(content, bytes):
        size = len(content)
        # Make the content a file-like object for simplicity
        content = BytesIO(content)
    elif type(content, BufferedReader):
        size = stat(content.fileno()).st_size
    # Add one to the size for the padding byte
    iterations = ceil((size + 1) / block_size)
    i = 0
    while i < (iterations - 1):  # iterations except the final block
        raw_data = content.read(block_size)
        encrypted_block, reference, key = \
            encrypt_block(raw_data, convergent_secret)
        blocks.append(encrypted_block)
        rk_pairs.append((reference, key))
        i += 1

    # We're at the last block
    raw_data = content.read(block_size)
    padded = pad(raw_data, block_size)
    encrypted_block, reference, key = \
        encrypt_block(padded, convergent_secret)
    blocks.append(encrypted_block)
    rk_pairs.append((reference, key))
    return blocks, rk_pairs


def encode(content, convergent_secret, block_size):
    """Encode and store ERIS data

    Args:
        content (bytes): The data to be encoded
        convergent_secret (bytes): The convergent secret used hash
        block_size (int): The size of the data blocks
    Returns:
        tuple: A tuple consisting of the data blocks (bytes), the
        block size, the number of levels of the tree, a reference to
        the root of the tree, and the key of the root node
    """
    blocks = []
    level = 0
    level_0_blocks, rk_pairs = \
        split_content(content, convergent_secret, block_size)
    blocks += level_0_blocks
    while len(rk_pairs) > 1:
        level_blocks, rk_pairs = \
            collect_rk_pairs(rk_pairs, convergent_secret, block_size)
        blocks += level_blocks
        level += 1
    root_reference, root_key = rk_pairs[0]
    return blocks, block_size, level, root_reference, root_key


def read_rk_pairs_from_node(node):
    """Splits the reference/key pairs from a data node

    Args:
       node (bytes): The node containing the reference/key pairs
    Returns:
       list: A list of keypairs for the node
    """
    output = []
    # Let's use the BytesIO library here to make our life easier
    with BytesIO(node) as content:
        while refkey := content.read(KEYPAIR_LENGTH):
            output.append((refkey[:32], refkey[32:]))
    return output


def decode_recurse(level, reference, key):
    """Decodes a tree of data by note

    Args:
        level (int): The level of tree, starting a 0 as root
        reference (bytes): The hash of the node
        key (bytes): The decryption key
    Returns:
        bytes: The decrypted plaintext
    """
    if level == 0:
        encrypted_content_block = get(reference)
        return ChaCha20.new(key=key).decrypt(encrypted_content_block)
    encrypted_node = get(reference)
    node = ChaCha20.new(key=key).decrypt(encrypted_node)
    output = []
    for sub_reference, sub_key in read_rk_pairs_from_node(node):
        output.append(decode_recurse(level - 1, sub_reference, sub_key))

        return b''.join(output)


def decode(block_size, level, root_reference, root_key):
    """Decode a level of data at a time

    Args:
        block_size (int): The size of the node blocks
        level (int): The level of the tree
        root_reference (bytes): Reference the root node of the tree/branch
        root_key (bytes): The decryption key for the reference node
    Returns:
        bytes: A decyrpted, unpadded block of data corresponding to the tree
    """
    padded = decode_recurse(level, root_reference, root_key)
    return unpad(padded, block_size)


def get(reference):
    """Retrieve the reference hash from block level storage

    Args:
       reference (bytes): The hash of the block to retrieve
    Returns:
       bytes: The block if it is in storage, otherwise None
    """
    # Get raw block from storage
    return db.get(reference)


def put(content, key=None):
    """Place the content into block level storage

    Args:
       content (bytes): The data to be stored
       key (:obj: 'bytes', optional): The reference for the object
    Returns:
       bytes: The reference for the object
    """
    # Put raw block into storage
    if not key:
        key = blake2b(content, digest_size=32).digest()
    db.put(key, content)
    return key
