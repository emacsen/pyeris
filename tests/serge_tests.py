from math import ceil
import sys
import os
import pytest
import json
import base64
import pickle
from hashlib import blake2b
from base64 import b32encode, b32decode, b64encode, b64decode
import nacl.hash
import eris.core
import eris.utils

data = json.load(open('tests/serge-tests.json'))
CONTENT = data['content'].encode()
BLOCK_SIZE = int(data['block_size'])
CONVERGENCE_SECRET = data['convergence_secret'].encode()

def test_basichash():
    data = b"test"
    target_hash = b"SKFSANTJIPRK7UI6XQHK4LSTVE57C55E7TZVXTDE2UBXATTF4IBA"
    blake_hash = eris.utils.blake2b_digest(data)
    assert eris.utils.unpad(blake_hash) == target_hash

def test_hashdata():
    """Test hash-data from wisperis

    test-equal "SKFSANTJIPRK7UI6XQHK4LSTVE57C55E7TZVXTDE2UBXATTF4IBA"
    ;; checked via python:
    base64.b32encode(libnacl.crypto_generichash("test".encode()))
    base32-encode : hash-data : string->utf8 "test"
    """
    
    data = b"test"
    wisp_digest = b"SKFSANTJIPRK7UI6XQHK4LSTVE57C55E7TZVXTDE2UBXATTF4IBA"
    blake_hash = eris.utils.blake2b_digest(data)
    assert eris.utils.unpad(blake_hash) == wisp_digest
    
def test_pad():
    data = b'AAAA'
    padded = eris.core.pad(data, 10)
    assert len(padded) is 10
    assert padded[4] == 0x80
    assert padded[5] == 0x00

def test_unpad():
    data = b'AAAA'
    padded = eris.core.pad(data, 10)
    unpadded = eris.core.unpad(padded, 10)
    assert data == unpadded

def test_fill_with_null_rk_pairs():
    nullpair = (b'\0' * 32, b'\0' * 32)    
    sample_ref = b'1' * 32
    sample_key = b'A' * 32
    arity_length = 4
    data = [(sample_ref, sample_key), (sample_ref, sample_key)]
    filled = eris.core.fill_with_null_rk_pairs(data, arity_length)
    # Ensure the arity if filled and contains nulls
    assert len(filled) == arity_length
    assert filled[2] == nullpair

def test_incorrect_fill_with_null_pk_pairs():
    sample_ref = b'1' * 32
    sample_key = b'A' * 32
    sample_rk = (sample_ref, sample_key)
    arity_length = 4

    # Test when there are too many rks
    data = [sample_rk] * 6
    filled = eris.core.fill_with_null_rk_pairs(data, arity_length)
    assert len(filled) == 6
    assert filled[-1] == sample_rk

    # Test when data is divisible by arity
    data = [sample_rk] * 8
    filled = eris.core.fill_with_null_rk_pairs(data, arity_length)
    assert len(filled) == 8
    assert filled[-1] == sample_rk
    
def test_encrypt_block():
    padded = eris.core.pad(CONTENT, BLOCK_SIZE)
    eb_block, eb_ref, eb_key = \
        eris.core.encrypt_block(padded, CONVERGENCE_SECRET)
    # Verify this test
    assert base64.b32encode(eb_ref) == \
        b'42G2VAYEUGBAVIABDTDMBMVA3GSPFFY67VKVFVVZ6AUHZV5FKMUOJOHGXZJHV4E5FX46PD5HIPPTEMLLOYZ35TVLMBSH7P4DAJHADUY='
    assert base64.b32encode(eb_key) == \
        b'LZEOQYJYGSNISIYDRTEPP52X5LXH5TTODPGHC3K7CAGUIXJZTN4A===='
    # This is the hash without the convergence secret
    assert eris.utils.blake2b_digest(eb_block) == \
        b'VASWEXD23G7RKTSK7KROLZHRYLV2P33SCDCMZJBGB3HTZ6JARDQA===='

def test_collect_rk_pairs():
    sample_ref = b'1' * 32
    sample_key = b'A' * 32
    sample_rk = (sample_ref, sample_key)
    sample_rk_pairs = [sample_rk] * 2
    convergence_secret = b'test'
    block_size = 128
    result_blocks, result_rk_pairs = \
        eris.core.collect_rk_pairs(sample_rk_pairs, convergence_secret, block_size)
    assert isinstance(result_blocks, list)
    assert isinstance(result_rk_pairs, list)
    tmp_block = result_blocks[0]
    # Resulting blocks should be the size of blocks
    assert len(tmp_block) == block_size
    # The number of rk pairs should be the number of blocks
    assert len(result_rk_pairs) == len(result_blocks)
    # TODO Add more tests

def test_split_content():
    # Don't spill into two blocks
    blocks, refkey_pairs = \
        eris.core.split_content(CONTENT, CONVERGENCE_SECRET, BLOCK_SIZE)
    assert len(blocks) == len(refkey_pairs) == 1
    padded = eris.core.pad(CONTENT, BLOCK_SIZE)
    manual_block, manual_ref, manual_key = eris.core.encrypt_block(padded, CONVERGENCE_SECRET)
    assert blocks[0] == manual_block

def test_read_rk_pair_from_node():
    ref = b'1' * 32
    key = b'A' * 32
    refkey = ref + key
    node = refkey * 10
    rk_pairs = eris.core.read_rk_pairs_from_node(node)
    assert len(rk_pairs) == 10
    assert rk_pairs[0] == (ref, key)




