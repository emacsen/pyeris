from math import ceil
import sys
import os
import pytest
import json
import base64
import pickle
from hashlib import blake2b
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from base64 import b32encode, b32decode
import nacl.hash
import eris.core
import eris.utils

data = json.load(open('tests/eris-test-vector-00.json'))
CONTENT = b'Hello world!'
BLOCK_SIZE = data['block-size']
CONVERGENT_SECRET = data['convergence-secret'].encode()

def test_content():
    assert eris.utils.unpad(base64.b32encode(CONTENT)) == \
        data['content'].encode()

def test_encode():
#    encoded_content = eris.utils.pad(b32encode(CONTENT))
    blocks, bsize, level, root_ref, root_key = \
        eris.core.encode(CONTENT, CONVERGENT_SECRET, BLOCK_SIZE)
    assert len(blocks) == 1
    assert bsize == BLOCK_SIZE
    assert level == 0
    assert b32encode(root_key) == data['read-capability']['root-key']
    assert root_ref == data['read-capability']['root-reference']
    assert blocks == data['blocks']


def test_read_rk_pair_from_node():
    ref = b'1' * 32
    key = b'A' * 32
    refkey = ref + key
    node = refkey * 10
    rk_pairs = eris.core.read_rk_pairs_from_node(node)
    assert len(rk_pairs) == 10
    assert rk_pairs[0] == (ref, key)


def decode_recurse():
    pass

def decode():
    pass







