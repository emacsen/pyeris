import setuptools

VERSION = '0.0.3'

with open("README", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pyeris-emacsen",
    version=VERSION,
    author="Serge Wroclawski",
    author_email="serge@wroclawski.org",
    description="A Python implementation of ERIS",
    long_description=long_description,
    long_description_content_type="text/plain",
    url="https://gitlab.com/emacsen/pyeris",
    packages=setuptools.find_packages(),
    license='Apache License 2.0',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
)
